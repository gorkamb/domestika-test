# Domestika test    
I did the test with graphql. I worked before with graphql a bit and I know
after the conversation you are using it. I think that I can better organization
with the folders... Im new in rails so I have to learn a lot yet....

Start:
```
rails serve
```

Open http://localhost:3000/graphiql. Check is it working...

I installed rubocop
```
rubocop
```

I did some test
```
rails db:reset
rails test
```

I created pipeline in bitbucket to run this thing in each commit.

Examples:
```
# Get all teachers 
query {
  teachers {
    id
    firstName
    lastName
    email
    votes
    courses {
      title
      description
      votes
    }
  }
}


# Find teacher
query {
  teacher(id: 1) {
    id
    firstName
    lastName
    email
    votes
    courses {
    	title
      	description
      	votes
    }
  }
}

# Create new teacher
mutation {
  createTeacher(input: { params: { firstName: "Gorka", lastName: "Mendez", email: "gorkamb@gmail.com"  }}) {
    teacher {
      id
      firstName
      lastName,
      email
    }
  }
}

# Show created teacher...
query {
  teacher(id: 11) {
    id
    firstName
    lastName
    email
    votes
    courses {
    	title
      	description
      	votes
    }
  }
}

# Create course
mutation {
  createCourse(input: { params: { title: "Graphql with rails", description: "try it...", userId: "11"  }}) {
    course {
      id
      title
      description
    }
  }
}

# Show created course...
query {
  teacher(id: 11) {
    id
    firstName
    lastName
    email
    votes
    courses {
    	title
      	description
      	votes
    }
  }
}

# Increment teacher vote. Warning. I think that with sqlite is not possible todo like this
mutation {
  incrementTeacherVote(input: { id: 11}) {
    teacher {
      votes
    }
  }
}

# Increment teacher vote. Warning. I think that with sqlite is not possible todo like this
mutation {
  incrementCourseVote(input: { id: 11}) {
    course {
      votes
    }
  }
}
```

