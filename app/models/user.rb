# frozen_string_literal: true

class User < ApplicationRecord
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :votes, numericality: { only_integer: true }, allow_blank: true

  has_many :courses
end
