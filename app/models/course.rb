# frozen_string_literal: true

class Course < ApplicationRecord
  validates :title, presence: true, uniqueness: true
  validates :description, presence: false
  validates :votes, numericality: { only_integer: true }, allow_blank: true

  belongs_to :user
end
