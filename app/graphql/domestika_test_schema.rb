# frozen_string_literal: true

class DomestikaTestSchema < GraphQL::Schema
  mutation(Types::MutationType)
  query(Types::QueryType)
end
