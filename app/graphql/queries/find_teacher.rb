# frozen_string_literal: true

module Queries
  class FindTeacher < Queries::BaseQuery
    type [Types::TeacherType], null: false
    argument :id, ID, required: true

    def resolve(id:)
      User.where(id: id)
    rescue ActiveRecord::RecordNotFound => _e
      GraphQL::ExecutionError.new('Teacher does not exist.')
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid attributes for #{e.record.class}:"\
        " #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
