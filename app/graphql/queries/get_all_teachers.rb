# frozen_string_literal: true

module Queries
  class GetAllTeachers < Queries::BaseQuery
    type [Types::TeacherType], null: false

    def resolve
      User.left_joins(:courses).where.not(courses: { id: nil })
    end
  end
end
