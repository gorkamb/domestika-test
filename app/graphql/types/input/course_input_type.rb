# frozen_string_literal: true

module Types
  module Input
    class CourseInputType < Types::BaseInputObject
      argument :title, String, required: true
      argument :description, String, required: true
      argument :user_id, ID, required: true
    end
  end
end
