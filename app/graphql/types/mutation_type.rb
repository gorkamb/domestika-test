# frozen_string_literal: true

module Types
  class MutationType < Types::BaseObject
    field :create_teacher, resolver: Mutations::CreateTeacher
    field :create_course, resolver: Mutations::CreateCourse
    field :increment_teacher_vote, resolver: Mutations::IncrementTeacherVote
    field :increment_course_vote, resolver: Mutations::IncrementCourseVote
  end
end
