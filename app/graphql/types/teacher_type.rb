# frozen_string_literal: true

module Types
  class TeacherType < Types::BaseObject
    field :id, ID, null: false
    field :first_name, String, null: false
    field :last_name, String, null: false
    field :email, String, null: false
    field :votes, Int, null: false
    field :courses, [Types::CourseType], null: true
  end
end
