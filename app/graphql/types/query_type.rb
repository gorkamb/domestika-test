# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    field :teachers, resolver: Queries::GetAllTeachers
    field :teacher, resolver: Queries::FindTeacher
  end
end
