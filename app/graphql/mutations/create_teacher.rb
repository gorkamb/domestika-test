# frozen_string_literal: true

module Mutations
  class CreateTeacher < Mutations::BaseMutation
    argument :params, Types::Input::TeacherInputType, required: true

    field :teacher, Types::TeacherType, null: false

    def resolve(params:)
      create_teacher_params = Hash params

      begin
        user = User.create!(create_teacher_params)

        { teacher: user }
      rescue ActiveRecord::RecordInvalid => e
        GraphQL::ExecutionError.new("Invalid attributes for #{e.record.class}:"\
          " #{e.record.errors.full_messages.join(', ')}")
      end
    end
  end
end
