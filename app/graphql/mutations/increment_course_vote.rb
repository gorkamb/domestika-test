# frozen_string_literal: true

module Mutations
  class IncrementCourseVote < Mutations::BaseMutation
    argument :id, ID, required: true

    field :course, Types::CourseType, null: false

    def resolve(id:)
      Course.increment_counter(:votes, id, touch: true)
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid attributes for #{e.record.class}:"\
        " #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
