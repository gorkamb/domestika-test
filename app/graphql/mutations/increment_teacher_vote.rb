# frozen_string_literal: true

module Mutations
  class IncrementTeacherVote < Mutations::BaseMutation
    argument :id, ID, required: true

    field :teacher, Types::TeacherType, null: false

    def resolve(id:)
      User.increment_counter(:votes, id, touch: true)
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid attributes for #{e.record.class}:"\
        " #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
