# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'should not create user without required fields' do
    user = User.new
    assert_not user.save

    user.first_name = 'first name'
    assert_not user.save

    user.last_name = 'last name'
    assert_not user.save

    user.email = 'unique_email@email.com'
    assert user.save
  end

  test 'email must be unique' do
    user = User.new
    user.first_name = 'first name'
    user.last_name = 'last name'
    user.email = 'unique_email@email.com'
    assert user.save

    user2 = User.new
    user2.first_name = 'first name 2'
    user2.last_name = 'last name 2'
    user2.email = 'unique_email@email.com'
    assert_not user2.save
  end
end
