# frozen_string_literal: true

require 'test_helper'

class CourseTest < ActiveSupport::TestCase
  test 'should not create course without required fields' do
    course = Course.new
    assert_not course.save

    course.title = 'super course title'
    assert_not course.save

    course.description = 'super course description'
    assert_not course.save

    user = User.new
    user.first_name = 'first name 2'
    user.last_name = 'last name 2'
    user.email = 'unique_email2@email.com'
    assert user.save

    course.user_id = user.id
    assert course.save
  end

  test 'title must be unique' do
    course = Course.new
    course.title = 'super course title'
    course.description = 'super course description'
    assert_not course.save
  end
end
