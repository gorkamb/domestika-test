# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'
10.times do
  first_name = Faker::Name.first_name
  last_name = Faker::Name.name
  email = Faker::Internet.unique.email
  votes = rand(1..100)
  User.create(first_name: first_name, last_name: last_name, email: email, votes: votes)
end

5.times do
  title = Faker::Book.unique.title
  description = Faker::Lorem.paragraph(sentence_count: 5)
  user_id = rand(1..10)
  votes = rand(1..100)
  Course.create(title: title, description: description, user_id: user_id, votes: votes)
end
