# frozen_string_literal: true

class AddedUserForeignKeyToCourse < ActiveRecord::Migration[6.0]
  def change
    add_foreign_key :courses, :users, on_delete: :cascade
  end
end
